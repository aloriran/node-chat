import Koa from 'koa';
import config from 'config';
import err from './middleware/error'
import {routes, allowedMethods} from './middleware/routes';
  import send from 'koa-send'

var mongoose = require('mongoose');
mongoose.connect(config.db.host);

const app = new Koa();

app.use(err);


app.use(routes());
app.use(allowedMethods());

app.listen(config.server.port, () =>
  console.log(`${config.app.name} listening at port ${config.server.port}`)
);

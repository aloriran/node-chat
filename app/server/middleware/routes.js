import Router from 'koa-router';
import convert from 'koa-convert';
import KoaBody from 'koa-body';
import User from '../model/user'
import {needAuth, getUser} from './auth'
import send from 'koa-send'


const router = new Router();
const koaBody = convert(KoaBody());

router
  .get('/api/users', async (ctx, next) => {
    let users = await User.find();
    ctx.body = users;
  })
  .post('/api/register', koaBody, async (ctx, next) => {
    let username = ctx.request.body.username;
    let password = ctx.request.body.password;
    console.log(username, password);
    let user = new User({ username: username});
    user.setupPassword(password);
    await user.save();
    console.log("was saved");
    ctx.body = { success: true, token: user.generateToken() };
  })
  .post('/api/login', koaBody, async (ctx, next) => {
    let username = ctx.request.body.username;
    let password = ctx.request.body.password;
    let user = await User.findOne({ username: username });
    console.log(user);
    if (user !== undefined && user.validatePassword(password)) {
      ctx.body = { success: true, token: user.generateToken() };
    } else {
      ctx.body = { success: false };
    }
  })
  //TODO move serving static files from here
  .get('/public/*', async (ctx, next) => {
    console.log("public", ctx.path);
    await send(ctx, ctx.path);
  })
  .get('/*', async (ctx, next) => {
    console.log("all", ctx.path);
    await send(ctx, '/public/index.html');
  });

export function routes() { return router.routes(); }
export function allowedMethods() { return router.allowedMethods(); }

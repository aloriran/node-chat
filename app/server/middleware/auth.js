import jwt from 'jsonwebtoken'
import config from 'config'

export function needAuth(ctx, next) {
  if (isAuthenticated(ctx)) {
    next();
  } else {
    ctx.status = 401;
  }
}

export function isAuthenticated(ctx) {
  return getUser(ctx) !== undefined;
}

export function getUser(ctx) {
  let token = ctx.request.header['auth-token'];
  console.log(token);
  if (token === undefined) {
    return undefined;
  }
  try {
    let user = jwt.verify(token, config.app.token);
    return user;
  } catch (e) {
    console.log(e);
    return undefined;
  }
}

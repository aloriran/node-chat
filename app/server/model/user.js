import mongoose from 'mongoose'
import bcrypt from 'bcryptjs'
import config from 'config'
import jwt from 'jsonwebtoken'


let User = new mongoose.Schema({
  username: {type: String, required: true, unique: true},
  passwordHash: {type: String}
});


User.methods.setupPassword = async function (password) {
  let user = this;
  let hash = await new Promise((resolve, reject) =>{
    bcrypt.hash(password, 10, (err, hash) => {
      console.log(err, hash);
      if (err) {
        return reject(err);
      }
      return resolve(hash);
    });
  });
  console.log(hash);
  user.passwordHash = hash;
};

User.methods.validatePassword = async function (password) {
  let user = this;
  return await bcrypt.compare(password, user.password);
};

User.methods.generateToken = function generateToken () {
  const user = this;
  return jwt.sign({ id: user.id, username: user.username}, config.app.token);
}

export default mongoose.model('user', User);

import { NgModule, Component, Input, Attribute } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ActivatedRoute, RouterModule } from '@angular/router';
import 'rxjs/add/operator/map';


@Component({
  selector: 'hello',
  template: '<h1>Hello, world!</h1>',
})
export class Hello {
}

@Component({
  selector: 'chat-app',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ChatApp {
}

const routing = RouterModule.forRoot([
  { path: '', component: Hello },
]);

@NgModule({
  imports: [
    BrowserModule,
    routing,
  ],
  declarations: [
    ChatApp,
    Hello
  ],
  providers: [

  ],
  bootstrap: [ChatApp],
})
export class AppModule {
}

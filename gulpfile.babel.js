import gulp from 'gulp';
import gutil, { PluginError } from 'gulp-util';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import sourcemaps from 'gulp-sourcemaps';

import assign from 'object-assign';
import browserify from 'browserify';
import watchify from 'watchify';
import babelify from 'babelify';
import {spawn} from 'child_process'

import del from 'del';

var nodeInstance;

gulp.task('copy', () => {
  return gulp.src(['app/client/index.html'])
    .pipe(gulp.dest('public'));
});

gulp.task('build', ['copy'], () => {
  const b = browserify('app/client/index.js', { debug: true })
    .transform(babelify);
  return bundle(b);
});

gulp.task('watch-client', () => {
  const b = browserify('app/client/index.js', assign({ debug: true }, watchify.args))
    .transform(babelify);
  const w = watchify(b)
    .on('update', () => bundle(w))
    .on('log', gutil.log);
  return bundle(w)
});

gulp.task('server', function() {
  if (nodeInstance){
    nodeInstance.kill();
  }
  console.log("Restarted server!");

  nodeInstance = spawn('node', ['start-server.js'], {stdio: 'inherit'});
  nodeInstance.on('close', function (code) {
    if (code === 8) {
      gulp.log('Error detected, waiting for changes...');
    }
  });
})

gulp.task('watch-server', () => {
  return gulp.watch(['./app/server/*/*.js', './app/server/*.js'], ['server']);
});

process.on('exit', function() {
  if (nodeInstance) {
    nodeInstance.kill();
  }
});

gulp.task('clean', () => {
  return del('public');
});

gulp.task('default', ['copy', 'watch-client', 'server', 'watch-server']);

function bundle(b) {
  return b.bundle()
    .on('error', (e) => {
      console.error(e.stack);
    })
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('public'));
}
